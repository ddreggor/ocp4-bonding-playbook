# Bonding machine config playbook

This playbook creates the MachineConfig (mc) file for bonding/LACP.  
It should be included inside `${CLUSTERNAME}/openshift` as a manifest.


To use it, add the details in `group_vars/all.yaml` and run the playbook as:

*Example:*
```
ansible-playbook --extra-vars='type=master' main.yml
ansible-playbook --extra-vars='type=edge' main.yml
```

*NOTE:*
The playbook was developed to use edge and core as worker types. You can modify the `main.yaml` and the `groups/all.yaml` to change the types. Specifically looking at the nic pool names (`MachineConfigPoolsEdge` or `MachineConfigPoolsCore`), the role names (`worker-edge` or `worker-core`) in the pools, and the member list name at the top of the `groups/all.yaml` (`nicsEdge` or `nicsCore`) as well as the pool called in the loop line main.yaml file.
